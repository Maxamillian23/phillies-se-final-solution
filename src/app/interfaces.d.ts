declare namespace parkingrequestapp {
    export interface IRequestForm {
        id: number;
        name: string;
        date: string;
        comments: string;
        requestDateId: number;
        approved: boolean;
    }

    export interface IEventForm {
        id: number;
        eventName: string;
        eventDateTime: string;
    }
}

export = parkingrequestapp;