import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ParkingDatabaseModule } from 'projects/parking-database/src';
import { environment } from 'src/environments/environment';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import {
  IgxNavigationDrawerModule,
  IgxButtonModule,
  IgxIconModule,
  IgxRippleModule,
  IgxDropDownModule,
  IgxSnackbarModule,
  IgxInputGroupModule,
  IgxPrefixModule,
  IgxSuffixModule,
  IgxGridModule,
  IgxSelectModule,
  IgxDialogModule,
  IgxAvatarModule,
  IgxCardModule,
  IgxCheckboxModule,
  IgxComboModule,
  IgxDatePickerModule,
  IgxExpansionPanelModule,
  IgxRadioModule,
  IgxSwitchModule,
  IgxTimePickerModule,
  IgxToggleModule
} from 'igniteui-angular';
import { RequestComponent } from './request/request.component';
import { RequestCreateComponent } from './request/request-create/request-create.component';
import { RequestEditComponent } from './request/request-edit/request-edit.component';
import { RequestFormComponent } from './request/request-form/request-form.component';
import { RequestTableComponent } from './request/request-table/request-table.component';
import { SearchComponent } from './common-components/search/search.component';
import { DateTimeSelectComponent } from './common-components/date-time-select/date-time-select.component';
import { GlobalMessageService } from './_services/global-message.service';
import { ScreenResizeEventService } from './_services/screen-resize-event.service';
import { UnapprovedRequestsComponent } from './unapproved-requests/unapproved-requests.component';
import { EventComponent } from './event/event.component';
import { EventCreateComponent } from './event/event-create/event-create.component';
import { EventEditComponent } from './event/event-edit/event-edit.component';
import { EventFormComponent } from './event/event-form/event-form.component';
import { EventTableComponent } from './event/event-table/event-table.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RequestComponent,
    RequestCreateComponent,
    RequestEditComponent,
    RequestFormComponent,
    RequestTableComponent,
    SearchComponent,
    DateTimeSelectComponent,
    UnapprovedRequestsComponent,
    EventComponent,
    EventCreateComponent,
    EventEditComponent,
    EventFormComponent,
    EventTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    IgxAvatarModule,
    IgxGridModule,
    IgxButtonModule,
    IgxCardModule,
    IgxCheckboxModule,
    IgxComboModule,
    IgxDatePickerModule,
    IgxDialogModule,
    IgxDropDownModule,
    IgxExpansionPanelModule,
    IgxIconModule,
    IgxInputGroupModule,
    IgxRadioModule,
    IgxRippleModule,
    IgxSelectModule,
    IgxSnackbarModule,
    IgxSwitchModule,
    IgxTimePickerModule,
    IgxToggleModule,
    IgxNavigationDrawerModule,
    ParkingDatabaseModule.forRoot(environment.database)
  ],
  providers: [
    EventService,
    RequestService,
    GlobalMessageService,
    ScreenResizeEventService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
