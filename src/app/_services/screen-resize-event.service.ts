import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScreenResizeEventService {

  // tslint:disable-next-line: variable-name
  private _resizeEvent = new Subject<string>();
  resizeEvent$ = this._resizeEvent.asObservable();

  constructor() { }

  public next(resizeEvent: string) {
    this._resizeEvent.next(resizeEvent);
  }
}
