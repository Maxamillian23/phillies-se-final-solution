import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalMessageService {

  // tslint:disable-next-line: variable-name
  private _message = new Subject<string>();
  message$ = this._message.asObservable();

  constructor() { }

  public next(newMessage: string) {
    this._message.next(newMessage);
  }
}
