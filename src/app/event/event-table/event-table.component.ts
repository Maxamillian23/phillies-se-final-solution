import { Component, OnInit, ViewChild } from '@angular/core';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import { IEvent } from 'projects/types/parking-requests/interfaces';
import { IgxGridComponent, IgxDialogComponent } from 'igniteui-angular';
import { ScreenResizeEventService } from 'src/app/_services/screen-resize-event.service';
import { Router } from '@angular/router';

export interface IEventTable {
  id: number;
  eventName: string;
  eventDateTime: Date;
}

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.scss']
})
export class EventTableComponent implements OnInit {

  @ViewChild('eventsGrid', { static: false }) grid: IgxGridComponent;
  @ViewChild('confirmDeleteDialog', { static: false }) confirmDeleteDialog: IgxDialogComponent;

  events: IEventTable[];

  constructor(
    public es: EventService,
    public gms: GlobalMessageService,
    public sres: ScreenResizeEventService,
    public router: Router
  ) {
    this.sres.resizeEvent$.subscribe(resized => {
      this.grid.reflow();
    });
  }

  ngOnInit() {
    this.es.getEvents().then(data => {
      this.events = this.formatData(data);
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to fetch events data');
    });
  }

  private formatData(e: IEvent[]): IEventTable[] {
    const result: IEventTable[] = [];
    e.forEach(elem => {
      const currEvent: IEventTable = {
        id: elem.id,
        eventName: elem.eventName,
        eventDateTime: new Date(elem.eventDate + 'T' + elem.eventTime)
      };
      result.push(currEvent);
    });
    return result;
  }

  confirmDeleteEvent(rowIndex: number) {
    this.confirmDeleteDialog.open();
    const sub = this.confirmDeleteDialog.onRightButtonSelect.subscribe(() => {
      const row = this.grid.getRowByIndex(rowIndex);
      const event: IEventTable = row.rowData;
      this.confirmDeleteDialog.close();
      this.es.deleteEvent(event.id).then(() => {
        row.delete();
      }).catch(error => {
        console.error(error);
        this.gms.next('Unable to delete row with id: ' + event.id);
      });
    });

    this.confirmDeleteDialog.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  editEvent(id: number) {
    this.router.navigateByUrl('/home/events/edit/' + id);
  }

  createEvent() {
    this.router.navigateByUrl('/home/events/create');
  }

}
