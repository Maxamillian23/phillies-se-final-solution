import { Component, OnInit, ViewChild } from '@angular/core';
import {
  IgxGridComponent,
  IgxDialogComponent
} from 'igniteui-angular';
import { IRequest, IEvent } from 'projects/types/parking-requests/interfaces';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import { ScreenResizeEventService } from 'src/app/_services/screen-resize-event.service';
import { Router } from '@angular/router';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';

export interface IRequestTable {
  id: number;
  name: string; // converted to JS Date Object
  date: Date;
  comments: string;
  requestDateId: number; // Join on this value to event data
  eventName?: string; // From Events Table
  eventDateTime?: Date; // From Events Table, converted to JS Date Object
  approved: boolean;
}

@Component({
  selector: 'app-request-table',
  templateUrl: './request-table.component.html',
  styleUrls: ['./request-table.component.scss']
})
export class RequestTableComponent implements OnInit {

  @ViewChild('requestsGrid', { static: false }) grid: IgxGridComponent;
  @ViewChild('confirmDeleteDialog', { static: false }) confirmDeleteDialog: IgxDialogComponent;

  // viewWidth = window.innerWidth - 40 + 'px';
  requests: IRequestTable[];

  constructor(
    public rs: RequestService,
    public es: EventService,
    public gms: GlobalMessageService,
    public sres: ScreenResizeEventService,
    public router: Router
  ) {
    this.sres.resizeEvent$.subscribe(resized => {
      this.grid.reflow();
    });
  }

  ngOnInit() {
    this.rs.getRequests()
      .then(requestData => {
        this.es.getEvents()
          .then(eventData => {
            this.requests = this.formatData(requestData, eventData);
          })
          .catch(error => {
            console.error(error);
            this.gms.next('Unable to fetch events data');
          });
      })
      .catch(error => {
        console.error(error);
        this.gms.next('Unable to fetch parking requests');
      });
  }

  private formatData(r: IRequest[], e: IEvent[]): IRequestTable[] {
    const result: IRequestTable[] = [];
    r.forEach(elem => {
      const currEntry: IRequestTable = {
        id: elem.id,
        name: elem.name,
        date: new Date(elem.date),
        comments: elem.comments,
        requestDateId: elem.requestDateId,
        approved: elem.approved
      };

      // Match the event to the request Date Id from the request
      const matchedEvent = e.find(evnt => evnt.id === currEntry.requestDateId);

      // Set the Event Date by splitting on dash, and creating a new date with the Y/m/d
      const eventDateParts = matchedEvent.eventDate.split('-');
      // tslint:disable-next-line: max-line-length
      currEntry.eventDateTime = new Date(parseInt(eventDateParts[0], 10), parseInt(eventDateParts[1], 10) - 1, parseInt(eventDateParts[2], 10));

      // Set the Event Time by splitting on colon, and setting each H/M/S
      const eventTimeParts = matchedEvent.eventTime.split(':');
      currEntry.eventDateTime.setHours(parseInt(eventTimeParts[0], 10));
      currEntry.eventDateTime.setMinutes(parseInt(eventTimeParts[1], 10));
      currEntry.eventDateTime.setSeconds(parseInt(eventTimeParts[2], 10));

      currEntry.eventName = matchedEvent.eventName;

      result.push(currEntry);
    });
    return result;
  }

  confirmDeleteRequest(rowIndex: number) {
    this.confirmDeleteDialog.open();
    const sub = this.confirmDeleteDialog.onRightButtonSelect.subscribe(() => {
      const row = this.grid.getRowByIndex(rowIndex);
      const request: IRequestTable = row.rowData;
      this.confirmDeleteDialog.close();
      this.rs.deleteRequest(request.id).then(() => {
        row.delete();
      }).catch(error => {
        console.error(error);
        this.gms.next('Unable to delete row with id: ' + request.id);
      });
    });

    this.confirmDeleteDialog.onClose.subscribe(() => {
      sub.unsubscribe();
    });
  }

  editRequest(id: number) {
    this.router.navigateByUrl('/home/requests/edit/' + id);
  }

  createRequest() {
    this.router.navigateByUrl('/home/requests/create');
  }

}
