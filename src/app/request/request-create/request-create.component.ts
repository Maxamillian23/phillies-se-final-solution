import { Component, OnInit } from '@angular/core';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import { Router } from '@angular/router';
import { IRequestForm } from 'src/app/interfaces';
import { IRequest } from 'projects/types/parking-requests/interfaces';
import { GlobalMessageService } from 'src/app/_services/global-message.service';

@Component({
  selector: 'app-request-create',
  templateUrl: './request-create.component.html',
  styleUrls: ['./request-create.component.scss']
})
export class RequestCreateComponent implements OnInit {

  constructor(public rs: RequestService, public router: Router, public gms: GlobalMessageService) { }

  ngOnInit() {
  }

  createRequest(requestForm: IRequestForm) {
    const newRequest: IRequest = {
      id: requestForm.id,
      name: requestForm.name,
      date: new Date().toISOString(),   // Set as the creation date
      comments: requestForm.comments,
      requestDateId: requestForm.requestDateId,
      approved: requestForm.approved
    };
    this.rs.createRequest(newRequest).then(() => {
      this.router.navigateByUrl('/home/requests');
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to create request');
    });
  }

}
