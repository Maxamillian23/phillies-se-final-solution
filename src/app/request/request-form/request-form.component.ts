import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IRequest, IEvent } from 'projects/types/parking-requests/interfaces';
import { IRequestForm } from 'src/app/interfaces';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { IgxDialogComponent } from 'igniteui-angular';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.scss']
})
export class RequestFormComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('selectedRequest') selectedRequest: IRequest;
  // tslint:disable-next-line: no-output-on-prefix no-output-rename
  @Output('onSubmit') onSubmit = new EventEmitter<IRequestForm>();

  @ViewChild('messageDialog', { read: IgxDialogComponent, static: false }) messageDialog: IgxDialogComponent;
  message: string;

  requestForm: FormGroup;
  events: IEvent[];

  constructor(public rs: RequestService, public es: EventService, public fb: FormBuilder, public gms: GlobalMessageService) {
    this.es.getEvents().then(data => {
      this.events = data;
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to load events');
    });
  }

  ngOnInit() {
    this.setForm(this.selectedRequest);
  }

  setForm(request?: IRequest) {
    if (!request) {
      // No request => Create Form
      this.requestForm = this.fb.group({
        id: [null, Validators.required],
        name: ['', Validators.required],
        date: [''],
        comments: [''],
        requestDateId: [null, Validators.required],
        approved: [false, Validators.required]
      });
    } else {
      // Is request => Edit Form
      this.requestForm = this.fb.group({
        id: [request.id, Validators.required],
        name: [request.name, Validators.required],
        date: [request.date, Validators.required], // This is only set on creation
        comments: [request.comments],
        requestDateId: [request.requestDateId, Validators.required],
        approved: [request.approved, Validators.required]
      });
      this.requestForm.controls.id.disable();
    }
  }

  submit(event) {
    this.onSubmit.emit(this.requestForm.value);
  }

}
