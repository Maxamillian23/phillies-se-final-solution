import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import { IRequest } from 'projects/types/parking-requests/interfaces';
import { IRequestForm } from 'src/app/interfaces';

@Component({
  selector: 'app-request-edit',
  templateUrl: './request-edit.component.html',
  styleUrls: ['./request-edit.component.scss']
})
export class RequestEditComponent implements OnInit {

  selectedRequest;
  selectedId;

  constructor(public route: ActivatedRoute, public router: Router, public rs: RequestService, public gms: GlobalMessageService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = +params.get('id');
      this.selectedId = id;
      this.rs
        .getRequestById(id)
        .then(r => {
          this.selectedRequest = r;
        })
        .catch(error => {
          console.error(error);
          this.gms.next('Could not load selected request');
        });
    });
  }

  updateRequest(requestForm: IRequestForm) {
    const updatedRequest: IRequest = {
      id: this.selectedId,
      name: requestForm.name,
      date: requestForm.date,
      comments: requestForm.comments,
      requestDateId: requestForm.requestDateId,
      approved: requestForm.approved
    };
    this.rs
      .updateRequest(updatedRequest)
      .then(() => {
        this.gms.next('Updated request');
        this.router.navigateByUrl('/home/requests');
      })
      .catch(error => {
        console.error(error);
        this.gms.next('Unable to update request');
      });
  }

}
