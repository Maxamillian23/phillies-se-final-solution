import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseModule } from 'projects/database/src/lib/database.module';
import { DatabaseService } from 'projects/database/src/lib/database.service';
import { DatabaseLocation, DbLocatorService } from './db-locator/db-locator.service';


@NgModule({
  imports: [CommonModule, DatabaseModule, HttpClientModule],
  providers: [DbLocatorService, DatabaseService]
})
export class ParkingDatabaseModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: ParkingDatabaseModule
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: DatabaseLocation): ModuleWithProviders {
    return {
      ngModule: ParkingDatabaseModule,
      providers: [{ provide: DatabaseLocation, useValue: config}]
    };
  }
}
