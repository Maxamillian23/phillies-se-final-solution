import { TestBed } from '@angular/core/testing';

import { DbLocatorService } from './db-locator.service';

describe('DbLocatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbLocatorService = TestBed.get(DbLocatorService);
    expect(service).toBeTruthy();
  });
});
