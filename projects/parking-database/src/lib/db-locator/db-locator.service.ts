import { Injectable } from '@angular/core';

@Injectable()
export class DatabaseLocation {
  url: string;
}

@Injectable()
export class DbLocatorService {
  // tslint:disable-next-line: variable-name
  private _apiEndpointUrl: string;

  constructor(location: DatabaseLocation) {
    this._apiEndpointUrl = location.url;
  }

  getLocation(): string {
    return this._apiEndpointUrl;
  }
}
