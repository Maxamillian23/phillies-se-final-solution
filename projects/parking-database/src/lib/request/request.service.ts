import { Injectable } from '@angular/core';
import { DatabaseService } from 'projects/database/src/lib/database.service';
import { DbLocatorService } from '../db-locator/db-locator.service';
import { IRequest } from 'projects/types/parking-requests/interfaces';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  // tslint:disable-next-line: variable-name
  _endpoint = '/requests';

  constructor(public ds: DatabaseService, public dbls: DbLocatorService) { }

  public async getRequests(): Promise<IRequest[]> {
    return this.ds.get(this.dbls.getLocation() + this._endpoint);
  }

  public async getRequestById(id: number): Promise<IRequest> {
    return this.ds.get(this.dbls.getLocation() + this._endpoint + `/${id}`);
  }

  public async createRequest(event: IRequest): Promise<IRequest> {
    return this.ds.post(this.dbls.getLocation() + this._endpoint, event);
  }

  public async updateRequest(req: IRequest): Promise<any> {
    return this.ds.update(this.dbls.getLocation() + this._endpoint + `/${req.id}`, req);
  }

  public async deleteRequest(id: number): Promise<any> {
    return this.ds.delete(this.dbls.getLocation() + this._endpoint + `/${id}`);
  }
}
